<?php
$conexion = mysqli_connect('localhost','root','','tareas');
require 'app/modelos/tarea.php';
?>

<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <title>Tareas</title>
</head>
<body style="background-color: blueviolet;">
<br><br>
<div class="col-md-12">
    <h1><center>Administracion de tareas<center></h1>
    <br><br>
</div>
<div class="col-md-12">
    <center><button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#staticBackdrop">
            Agregar
        </button>
        <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#actualiza">
            Actualizar Datos
        </button>
        <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#Eliminar">
            Eliminar
        </button><center><br><br>
</div>
<div class="col-md-12">
    <h3><center><table border="2px dashed" style="background-color: #82FBC6 ;">
                <tr>
                    <td> ID <td>
                    <td> Nombre tareas <td>
                    <td> Instrucciones <td>
                    <td> Materia <td>
                    <td> Fecha de entrega <td>
                </tr>
                <?php
                $sql = "SELECT * FROM Lista_Tareas";
                $result= mysqli_query($conexion,$sql);
                while($mostrar = mysqli_fetch_array($result)){

                    ?>
                    <tr>
                        <td><?php echo $mostrar['ID'] ?><td>
                        <td><?php echo $mostrar['NOMBRE'] ?><td>
                        <td><?php echo $mostrar['INSTRUCCIONES'] ?><td>
                        <td><?php echo $mostrar['MATERIA'] ?><td>
                        <td><?php echo $mostrar['FECHA_ENTREGA'] ?><td>
                    </tr>
                    <?php
                }
                ?>
            </table><center></h3>
    <br><br>
</div>
<div class="col-md-12" style="align-content: center;">
    <div class="row" class="container">
        <!-- Button trigger modal -->


        <!-- Modal -->
        <div class="modal fade" id="staticBackdrop" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Agregar datos</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="&c=Ctareas&a=enviar" method="POST">
                            <h2> CAPTURAR </h2>
                            Nombre: <input type="text" name="nombre" placeholder="Nombre tarea" required><br>
                            Instrucciones: <textarea type="text" name="instrucciones" placeholder="instrcciones" required></textarea><br>
                            Materia: <input type="text" name="materia" placeholder="Materia" required><br>
                            Fecha de entrega: <input type="datetime-local" name="entrega" placeholder="fecha entrega" required><br>
                            <a href="index.php"><input class="btn btn-success" type="submit" name="Enviar" value="Enviar"></a>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="actualiza" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Actualizar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2><center> Actualizar Datos </center></h2>
                        <form action="&c=Ctareas&a=editar" method="POST">
                            Id: <input type="text" name="id" placeholder="ingresa id" required><br>
                            Nombre: <input type="text" name="nombre" placeholder="Nombre de la tarea" required><br>
                            Instrucciones: <textarea type="text" name="instrucciones" placeholder="Instrucciones" required></textarea><br>
                            Materia: <input type="text" name="materia" placeholder="Materia" required><br>
                            Fecha de entrega: <input type="datetime-local" name="entrega" placeholder="fecha entrega" required><br>
                            <a href="index.php"><input class="btn btn-success" type="submit" name="Enviar" value="Enviar"></a>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="Eliminar" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="staticBackdropLabel">Eliminar</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h2><center> Eliminar Registros </center></h2>
                        <form action="&c=Ctareas&a=eliminar" method="POST" style="align-content: center;">
                            Id: <input type="text" name="Id" placeholder="Ingresa tu Id" required> <br><br>
                            <a href="index.php"><input class="btn btn-danger" type="submit" value="Eliminar Registro" name="ELiminar"></a>
                        </form>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>
</html>